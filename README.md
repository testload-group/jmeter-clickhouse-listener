# JMeter-Clickhouse-Listener

[![maven central](https://img.shields.io/maven-central/v/cloud.testload/jmeter-clickhouse-listener.svg?labelmaven%20central)](https://search.maven.org/search?q=g:%22cloud.testload%22%20AND%20a:%22jmeter-clickhouse-listener%22)
[![javadocs](https://www.javadoc.io/badge/cloud.testload/jmeter-clickhouse-listener.svg)](https://www.javadoc.io/doc/cloud.testload/jmeter-clickhouse-listener)
![pipeline](https://gitlab.com/testload-group/jmeter-clickhouse-listener/badges/master/pipeline.svg)

This JMeter Plugin (formely known as [jmeter-listener](https://gitlab.com/testload/jmeter-listener)) allows to write load test data on-the-fly to ClickHouse.

Explanations and usage examples on [project wiki](https://gitlab.com/testload/jmeter-clickhouse-listener/wikis/1.-Main). 

Strongly recommended use [clickhouse_bulk](https://github.com/nikepan/clickhouse-bulk) ([docker image](https://hub.docker.com/r/nikepan/clickhouse-bulk/)) - brilliant INSERT bulkizator for ClickHouse

Many thanks for support from:

[JetBrains](https://www.jetbrains.com/opensource/)

[Atlassian](https://www.atlassian.com/software/views/open-source-license-request)
